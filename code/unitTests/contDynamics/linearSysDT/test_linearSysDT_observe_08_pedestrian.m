function res = test_linearSysDT_observe_08_pedestrian()
% test_linearSysDT_observe_08_pedestrian - unit_test_function for guaranteed
% state estimation of linear discrete-time systems.
%
% Checks the solution of the linearSysDT class for a pedestrian example;
% It is checked whether the enclosing interval of the final observed set 
% is close to an interval provided by a previous solution that has been saved
%
% Syntax:  
%    res = test_linearSysDT_observe_08_pedestrian
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%    -
 
% Author:       Matthias Althoff
% Written:      03-Mar-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------


%% Load pedestrian model
load pedestrianModel pedestrian params options simRes


% Set of evaluated estimators
Estimator = {
    'VolMin-B' 
    'FRad-A' 
    'FRad-B' 
    'PRad-A' 
    'PRad-B' 
    'PRad-C-I' 
    'PRad-C-II' 
    'PRad-D' 
    'CZN-A'
    'CZN-B'
    'ESO-A'
    'ESO-B'
    'FRad-C' 
    'PRad-E' 
    'Nom-G' 
    'ESO-C'
    'ESO-D'
    'Hinf-G' 
    };




%% perform evaluation
% loop over estimators
for iEst = 1:length(Estimator)

    % set algorithm
    estName = Estimator{iEst};
    options.alg = estName;
    
    % if constrained zonotopes or ellipsoids are used
    paramsNew = params;
    if any(strcmp(estName,{'ESO-A','ESO-B','ESO-C','ESO-D','ESO-E'}))
        paramsNew.R0 = ellipsoid(params.R0);
        paramsNew.W = ellipsoid(params.W);
        paramsNew.V = ellipsoid(params.V);
    elseif any(strcmp(estName,{'CZN-A','CZN-B'}))
        paramsNew.R0 = conZonotope(params.R0);
    end
    
    % evaluate observer
    estSet{iEst} = evaluateSingleObserver(pedestrian,paramsNew,options);
    estName
    estSet{iEst}.Performance.IRadius

    % plot results
    if ~isempty(estSet{iEst})
        for dim = 1:4
            figure; hold on;
            % plot time elapse
            plotOverTime(estSet{iEst}.EstStates,dim,'FaceColor',[.6 .6 .6],'EdgeColor','none');
            % plot simulation
            plotOverTime(simRes,dim);

            % label plot
            xlabel('t');
            ylabel(['x_{',num2str(dim),'}']);
        end
    end
end


%------------- END OF CODE --------------
