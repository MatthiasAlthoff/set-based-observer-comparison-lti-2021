function res = test_linearSysDT_observe_01_vehicle()
% test_linearSysDT_observe_01_vehicle - unit_test_function for guaranteed
% state estimation of linear discrete-time systems.
%
% Checks the solution of the linearSysDT class for a vehicle example;
% It is checked whether the enclosing interval of the final observed set 
% is close to an interval provided by a previous solution that has been saved
%
% Syntax:  
%    res = test_linearSysDT_observe_01_vehicle()
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%    -
 
% Author:       Matthias Althoff
% Written:      08-Sep-2020
% Last update:  16-Jan-2021
% Last revision:---

%------------- BEGIN CODE --------------

% set path
savepath = [coraroot '/unitTests/contDynamics/linearSysDT/results'];
close all

% Parameters --------------------------------------------------------------

params.tStart = 0; % start time --> remove after fixing options checks
params.tFinal = 1; % final time
params.U = []; % input set

% Reachability Settings ---------------------------------------------------

options.zonotopeOrder = 100; % zonotope order
options.reductionTechnique = 'pca'; % reduction technique
options.timeStep = 0.01;

% Simulation Settings -----------------------------------------------------

options.points = 1;
options.p_conf = 0.999; % probability that sample of normal distribution within specified set


% vector of model dimensions
%dimVec = [2, 4, 6];
dimVec = 6;

% Set of evaluated estimators
Estimator = {
    'VolMin-B' 
    'FRad-A' 
    'FRad-B' 
    'PRad-A' 
    'PRad-B' 
    'PRad-C-I' 
    'PRad-C-II' 
    'PRad-D' 
    'CZN-A'
    'CZN-B'
    'ESO-A'
    'ESO-B'
    'FRad-C' 
    'PRad-E' 
    'Nom-G' 
    'ESO-C'
    'ESO-D'
    'Hinf-G' 
    };

% init
estSet = [];

%% perform evaluation
% loop over model dimensions
for iDim = 1:length(dimVec)
    % retrieve dimension
    dim = dimVec(iDim);
    
    % obtain system struct
    [sys, InpSig] = simulateVehicle(dim,params,options);
    
    % input
    params.uTrans = zeros(dim,1); %input transition --> remove after fixing options checks
    params.uTransVec = InpSig'; %input transition --> remove after fixing options checks
    
    % create system
    c = zeros(length(sys.A),1);
    vehicle = linearSysDT('vehicle',sys.A, sys.B, c, sys.C, options.timeStep); %initialize system
       
    % loop over estimators
    for iEst = 1:length(Estimator)

        % set algorithm
        estName = Estimator{iEst};
        options.alg = estName;

        %% Initial sets, disturbance sets, and noise sets
        if any(strcmp(estName,{'ESO-A','ESO-B','ESO-C',...
                'ESO-D','ESO-E'}))
            % ellipsoids
            params.R0 = ellipsoid(eye(size(sys.A,1)),zeros(size(sys.A,1),1)); % Initial State bounded in unity box
            params.W =  sys.E*ellipsoid(eye(size(sys.E,1)),zeros(size(sys.E,1),1)); 
            params.V =  sys.F*ellipsoid(eye(size(sys.C,1)),zeros(size(sys.C,1),1));
        else
            % zonotopes
            params.R0 = zonotope([zeros(size(sys.A,1),1),eye(size(sys.A,1))]); % Initial State bounded in unity box
            params.W = sys.E*zonotope([zeros(size(sys.E,1),1),eye(size(sys.E,1))]); 
            params.V = sys.F*zonotope([zeros(size(sys.C,1),1),eye(size(sys.C,1))]);
        end
        


        % simulate result assuming Gaussian distributions
        simRes = simulateGaussian(vehicle, params, options);

        % obtain output values
        for i=1:length(simRes.t{1})
            % create measurement noise
            v = randPointGaussian(params.V,options.p_conf);
            % obtain output value
            params.yVec(:,i) = sys.C*simRes.x{1}(i,:)' + v;
        end
        
        % if constrained zonotopes are used
        if any(strcmp(estName,{'CZN-A','CZN-B'}))
            params.R0 = conZonotope(params.R0);
        end

        %Volume Min -I Approach is only applied to two-dimensional
        %models due to high computational costs
        if strcmp(estName,'VolumeMinI')
            if dim == 2
                estSet{iEst} = evaluateSingleObserver(vehicle,params,options);
            end
        else
            % all estimators, except VolMinI
            estSet{iEst} = evaluateSingleObserver(vehicle,params,options);
        end
        
%         tic
%         for i = 1:length(estSet{iEst}.EstStates.timePoint.set)
%             % compute interval hull
%             I = interval(estSet{iEst}.EstStates.timePoint.set{i});
%         end
%         tCompInterval = toc;
%         tCompInterval_step = tCompInterval/length(estSet{iEst}.EstStates.timePoint.set);
    
        estSet{iEst}.Name
        estSet{iEst}.Performance.IRadius
        estSet{iEst}.tIteration*1e3
           
            
        % plot results
        if ~isempty(estSet{iEst})
            %for idim = 1:2
            for idim = 1:0
                figure; hold on;
                % plot time elapse
                plotOverTime(estSet{iEst}.EstStates,idim,'FaceColor',[.6 .6 .6],'EdgeColor','none');
                % plot simulation
                plotOverTime(simRes,idim);

                % label plot
                xlabel('t');
                ylabel(['x_{',num2str(idim),'}']);
            end

            % save results
            ZOrder = options.zonotopeOrder;
            save([savepath '/' 'Vehicle_' num2str(iDim) '_States_ZOrder_' num2str(ZOrder) '_' date '.mat'], 'dim', 'ZOrder', 'estSet')
        end
    end
end
clc;
end


%------------- END OF CODE --------------

