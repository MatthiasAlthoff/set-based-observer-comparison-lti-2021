function [sys,InpSig] = simulateVehicle(dim,params,options)
% simulateVehicle - simulates a vehicle subject to disturbances and sensor
% noise
%
% Syntax:  
%    [sys,options] = simulateVehicle(dim,ts,T)
%
% Inputs:
%    dim - system dimension
%    ts - time step size
%    T - time horizon
%    Disturbance - type of disturbance
%
% Outputs:
%    sys - system struct
%    options - options struct
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Matthias Althoff
% Written:      08-Sep-2020
% Last update:  10-Jan-2021
% Last revision:---

%------------- BEGIN CODE --------------

% generate vector of discrete points in time
tVec = 0:options.timeStep:params.tFinal-options.timeStep;

% assign nr of outputs to different vehicle models 
if dim == 2
    nrOfOutputs = 1;
elseif dim == 4
    nrOfOutputs =3;
elseif dim == 6
    nrOfOutputs =4;
end

%set sensor noise
Vk = 0.1*ones(1,6);
% set disturbance
Wk = [0.005,0.055,0.005,0.04,0.02,0.2];

%% vehicle dynamics
% vehicle paramters
mu=1; 
Vx = 15;
lr = 1.508; 
lf = 0.883; 
m =1.225e3 ; 
g = 9.81;  
Iz = 1.538e3;
Cf = 45000; 
Cr = 57000;  % These paramters have been taken from CommonRoad vehicle paramters considering friction = 1,  However if both values are same, then observability is lost;
Bs = 0.5; 
Js = 0.05; 
etat = 0.010; 
Rs = 16;
Fzf = m*g*lr/(lf+lr); % front tire vertical force
Fzr = m*g*lf/(lf+lr); % rear tire vertical force
Csf = Cf/(mu*Fzf);Csr =Cr/(mu*Fzr);
% system dynamics
C1 = mu/(Vx*(lr+lf)); C2 = mu*m/(Iz*(lr+lf)); 
VehParam.a11 = -C1*g*(Csr*lf-Csf*lr);
VehParam.a12 = C1*(g*lf*lr/Vx)*(Csr-Csf) - 1; 
VehParam.a21 = C2*g*lr*lf*(Csr-Csf); 
VehParam.a22 = -C2*(g/Vx)*(lf*lf*lr*Csf + lr*lr*lf*Csr);
VehParam.b11 = C1*g*lr*Csf; 
VehParam.b12 = C2*g*lf*lr*Csf; 
VehParam.a61 = etat*mu*Csf*Fzf/Js;
VehParam.a62 = etat*mu*Csf*Fzf*lf/(Js*Vx);
VehParam.a65 = -etat*mu*Csf*Fzf/Js;
VehParam.a66  = -Rs*Bs/Js;
VehParam.b16 = Rs/Js;
VehParam.Vx = Vx;
VehParam.IP = 2;


%% generate system matrices
if dim ==2 % 2 states model
    A = [VehParam.a11,VehParam.a12;VehParam.a21,VehParam.a22];
    B = [VehParam.b11;VehParam.b12];
    C = [0 VehParam.Vx]; % single measurement
elseif dim ==4 % 4 states model
    A = [VehParam.a11,VehParam.a12,0,0;VehParam.a21,VehParam.a22,0,0; 0,1,0,0; VehParam.Vx,VehParam.Vx,0,0];
    B = [VehParam.b11;VehParam.b12;0;0];
    C = [0 VehParam.Vx 0 0; 0 0 1 0; 0 0 0 1]; % 3 measurements
elseif dim==6 %(Comment: why are b-values in A matrix?)
    A =  [VehParam.a11,VehParam.a12,0,0, VehParam.b11,0; VehParam.a21,VehParam.a22,0,0, VehParam.b12,0; 0,1,0,0,0,0; VehParam.Vx,VehParam.Vx,0,0,0,0; 0,0,0,0,0,1;VehParam.a61,VehParam.a62,0,0,VehParam.a65,VehParam.a66];
    B = [0;0;0;0;0;VehParam.b16];
    C = [0 VehParam.Vx 0 0 0 0; 0 0 1 0 0 0; 0 0 0 1 0 0; 0 0 0 0 1 0]; % 4 measurements
end

%% generate input signal (double lane change)

% Max nr of samples for which the steer input is not zero (for non-zero curvature)
NCurv = 100; 

% (comment: why does Nmag1 depend on the dimension?)
if dim == 6
    Nmag1 = VehParam.IP; % The steer magnitude - Seg 1
else
    Nmag1 = 0.1*VehParam.IP;
end

% (comment: what is InpZero?)
InpZero = length(0:options.timeStep:params.tFinal)-2*NCurv-100;  % For two segments
% input signal
sig = [zeros(50,1);Nmag1*ones(NCurv,1);zeros(50,1);-Nmag1*ones(NCurv,1); zeros((InpZero),1)];
% cut-off input signal
InpSig = sig(1:length(tVec));

%% set system parameters and convert to discrete-time model parameters
% system parameters
sys.F = diag(Vk(1:nrOfOutputs));
sys.E = diag(Wk(1:dim));
PlantCT = ss(A,B,C,[]); % Continious Time Plant
PlantDT = c2d(PlantCT,options.timeStep,'impulse'); % Discretized Plant
PlantDT = ss(PlantDT.A,[PlantDT.B sys.E],PlantDT.C,0,options.timeStep); % Integrated Discretized New Plant with
sys.A = PlantDT.A; 
sys.B = PlantDT.B(:,1:end-size(sys.E,1));
sys.E = PlantDT.B(:,size(sys.B,2)+1:end);
sys.C = PlantDT.C;

% % set noise
% if strcmp(Disturbance,'random')
%     for i = 1:size(sys.C,1)
%         sys.vk(:,i) = 0 + 0.33*randn(length(tVec),1);
%         sys.vk(:,i) = SaturateNoise(sys.vk(:,i),-1,1);
%     end
% end
% 
% % generate process noise
% for i = 1:size(sys.A,1)
%     sys.wk(:,i) = 0 + 0.33*randn(length(tVec),1);
%     sys.wk(:,i) = SaturateNoise(sys.wk(:,i),-1,1);
% end
% 
% 
% %% simulate plant subject to process noise
% [Y,~,X] = lsim(PlantDT,[InpSig(1:length(tVec),1) sys.wk],tVec); % Simulate the Plant
% sys.Y = Y';
% sys.X = X';
% sys.U = InpSig';

end


%% function to saturate noise
function saturated_x = SaturateNoise(x,lb,ub)
saturated_x = min(ub, max(lb, x));
end

%------------- END OF CODE --------------