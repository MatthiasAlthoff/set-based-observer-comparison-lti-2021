function res = test_ellipsoid_supportFunc
% test_ellipsoid_supportFunc - unit test function of supportFunc
%
% Syntax:  
%    res = test_ellipsoid_supportFunc
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      18-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;
nRuns = 2;
bools = [false,true];
for i=10:5:15
    for j=1:nRuns
        for k=1:2
            E = ellipsoid.generateRandom(i,bools(k));
            % generate a few random directions
            N = 2*dim(E);
            V = randn(dim(E),N);
            % normalize
            V = V./sqrt(sum(V.^2,1));
            for m=1:N
                v = V(:,i);
                [val_u,x_u] = supportFunc(E,v);
                % check if "v" points in direction of x
                if v'*(x_u-E.q)<0
                    res = false;
                    break;
                end
                % check if x is a boundary point of E
                [~,d_rel] = containsPoint(E,x_u);
                if d_rel>1+E.TOL
                    res = false;
                    break;
                end
                % check if x attains val
                x_abs = max(abs(x_u));
                if max(abs(val_u-v'*x_u)/x_abs)>E.TOL
                    res = false;
                    break;
                end
                [val_l,x_l] = supportFunc(E,v,'lower');
                x_l_true = x_u - 2*(x_u-E.q);
                xl_abs = max(abs(x_l));
                if max(abs(x_l-x_l_true)/xl_abs)>E.TOL
                    res = false;
                    break;
                end
                % check if x_l attains val_l
                x_abs = max(abs(x_l));
                if max(abs(val_l-v'*x_l)/x_abs)>E.TOL
                    res = false;
                    break;
                end
            end
            if ~res
                break;
            end
        end
    end
end

if res
    disp('test_ellipsoid_supportFunc successful');
else
    disp('test_ellipsoid_supportFunc failed');
end

%------------- END OF CODE --------------
