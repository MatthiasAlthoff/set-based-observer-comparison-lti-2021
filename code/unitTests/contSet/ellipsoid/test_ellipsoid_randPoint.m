function res = test_ellipsoid_randPoint
% test_ellipsoid_randPoint - unit test function of test_ellipsoid_randPoint
%
% Syntax:  
%    res = test_ellipsoid_randPoint
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      18-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

runs = 10;
res = true;
bools = [false,true];
for i=1:5:20
    for j=1:runs
        N = 10*i;
        for k=1:2
            E = ellipsoid.generateRandom(i,bools(k));
            samples = randPoint(E,N);
            if ~all(containsPoint(E,samples))
                res = false;
                break;
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
if ~res
    disp('test_ellipsoid_randPoint failed');
else
    disp('test_ellipsoid_randPoint successful');
end

%------------- END OF CODE --------------