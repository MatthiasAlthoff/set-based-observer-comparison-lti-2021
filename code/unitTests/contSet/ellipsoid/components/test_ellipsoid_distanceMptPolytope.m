function res = test_ellipsoid_distanceMptPolytope
% test_ellipsoid_distanceMptPolytope - unit test function of test_ellipsoid_distanceMptPolytope
%
% Syntax:  
%    res = test_ellipsoid_distanceMptPolytope
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gaßmann
% Written:      18-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;
nRuns = 2;
bools = [false,true];
% smaller dimensions since vertices and halfspaces are involved
for i=5:5:10
    for j=1:nRuns
        for k=1:2 
            E = ellipsoid.generateRandom(i,bools(k));
            N = 2*i;
            % generate random point cloud center around origin
            V = randn(i,N);
            V = V - mean(V,2);
            B = boundary(E,N);
            m = ceil(N*rand);
            s = E.q + rand*(B(:,m)-E.q);
            r = ceil(i*rand);
            IntE = interval(E);
            IntV = interval(mptPolytope(V'));
            for m=1:2
                [Q,~] = qr(V);
                V = Q'*V;
                % for bools(m)=false, generate degenerate point cloud
                V(r,:) = bools(m)*V(r,:);
                V = Q*V;
                
                V1 = V + s;
                % intersects with E
                P1 = mptPolytope(V1');
                d1 = distance(E,P1);
                c_abs = max(max(abs(V1)));
                if d1/c_abs>E.TOL
                    res = false;
                    break;
                end
                
                % does not intersect E
                V2 = V+2*rad(IntE)+2*rad(IntV);
                P2 = mptPolytope(V2');
                d2 = distance(E,P2);
                c_abs = max(max(abs(V2)));
                if d2/c_abs<=E.TOL
                    res = false;
                    break;
                end
            end
            if ~res
                break;
            end
        end
        if ~res
            break;
        end
    end
    if ~res
        break;
    end
end
%------------- END OF CODE --------------