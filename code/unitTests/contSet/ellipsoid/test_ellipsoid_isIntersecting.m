function res = test_ellipsoid_isIntersecting
% test_ellipsoid_isIntersecting - unit test function of test_ellipsoid_isIntersecting
%
% Syntax:  
%    res = test_ellipsoid_isIntersecting
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      18-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% point
[~,res] = evalc('test_ellipsoid_containsPoint');

% all that implement distance
[~,res_] = evalc('test_ellipsoid_distance');
res = res && res_;

% mixed
res = res && test_ellipsoid_isIntersectingMixed;

if res
    disp('test_ellipsoid_isIntersecting successful');
else
    disp('test_ellipsoid_isIntersecting failed');
end
%------------- END OF CODE --------------