function res = test_ellipsoid_cartProd
% test_ellipsoid_cartProd - unit test function of cartProd
%
% Syntax:  
%    res = test_ellipsoid_cartProd
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      18-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

runs = 10;
res = true;
bools = logical([1,1,0,0;1,0,1,0]);
for j=1:runs
    for k=1:4
        E1 = ellipsoid.generateRandom(bools(1,k));
        n1 = dim(E1);
        E2 = ellipsoid.generateRandom(bools(2,k));
        n2 = dim(E2);
        E = cartProd(E1,E2);
        Y1 = randPoint(E1,dim(E1));
        Y2 = randPoint(E2,dim(E2));
        Y = combvec(Y1,Y2);
        if ~all(containsPoint(E,Y)) || ~in(project(E,1:n1),E1) ||...
           ~in(project(E,n1+1:n1+n2),E2)
            res = false;
            break;
        end
    end
    if ~res
        break;
    end
end
if ~res
    disp('test_ellipsoid_cartProd failed');
else
    disp('test_ellipsoid_cartProd successful');
end

%------------- END OF CODE --------------