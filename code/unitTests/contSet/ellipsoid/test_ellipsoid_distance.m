function res = test_ellipsoid_distance
% test_ellipsoid_distance - unit test function of test_ellipsoid_distance
%
% Syntax:  
%    res = test_ellipsoid_distance
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Ga�mann
% Written:      18-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res = true;

% ellipsoid
res = res && test_ellipsoid_distanceEllipsoid;

% conHyperplane
res = res && test_ellipsoid_distanceHyperplane;

% mptPolytope (halfspace is implicitly tested)
res = res && test_ellipsoid_distanceMptPolytope;

% double
res = res && test_ellipsoid_distancePoint;

if res
    disp('test_ellipsoid_distance successful');
else
    disp('test_ellipsoid_distance failed');
end
%------------- END OF CODE --------------