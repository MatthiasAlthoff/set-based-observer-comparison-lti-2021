function res_analytical = test_capsule_radius
% test_capsule_radius - unit test function of radius
%
% Syntax:  
%    res = test_capsule_radius
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Mark Wetzlinger
% Written:      28-August-2019
% Last update:  12-March-2021 (MW, add random tests)
% Last revision:---

%------------- BEGIN CODE --------------

% 1. Analytical test

% instantiate capsule
C = capsule([0;0],[1;0],0.5);

% compute enclosing radius
C_rad = radius(C);
% true solution
C_rad_true = 1.5;

% compare results
tol = 1e-9;
res_analytical = abs(C_rad - C_rad_true) < tol;


% 2. Random tests
res_rand = true;
for n=1:2:30
    % init random capsule with generator of length zero
    r = rand(1);
    C = capsule(randn(n,1),zeros(n,1),r);
    
    % since capsule is a ball, enclosing radius is radius
    if radius(C) ~= r
        res_rand = false; break;
    end
end


% combine results
res = res_analytical && res_rand;
if res
    disp('test_radius successful');
else
    disp('test_radius failed');
end

%------------- END OF CODE --------------