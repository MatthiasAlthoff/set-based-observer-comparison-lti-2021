function d = dim(obj)
% dim - return dimension of strip
%
% Syntax:  
%    d = dim(obj)
%
% Inputs:
%    obj - strips object
%
% Outputs:
%    d - dimension of the strip
%
% Example: 
%    S = strips([1 1],1);
%    dim(S)
%
% Other m-files required: center.m
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/dim
%
% Author:        Niklas Kochdumper
% Written:       23-November-2020
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

    d = size(obj.C,2);

%------------- END OF CODE --------------