function obj = mtimes(M,obj)
% mtimes - overloaded '*' operator for the multiplication of a matrix with
%          a strips
%
% Syntax:  
%    obj = mtimes(M,obj)
%
% Inputs:
%    M - numerical square matrix
%    obj - strips object
%
% Outputs:
%    obj - resulting strips object
%
% Example: 
%    M = [1 2; -1 1];
%    S = strips([1 1],1);
%    
%    res = M * S;
%
%    xlim([-5,5]); ylim([-5,5]);
%    plot(S,[1,2],'r');
%    plot(res,[1,2],'b');
%    xlim([-4,4]); ylim([-4,4]);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plus

% Author:       Niklas Kochdumper
% Written:      23-November-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % transform normal vectors
    C = obj.C/M;
    
    % construct resulting strips object
    obj = strips(C,obj.d,obj.y);

%------------- END OF CODE --------------