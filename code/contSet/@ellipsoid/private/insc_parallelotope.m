function Z = insc_parallelotope(E)
% insc_parallelotope - underapproximates an ellipsoid by a parellelotope
%
% Syntax:  
%    Z = insc_parallelotope(E)
%
% Inputs:
%    E - ellipsoid object
%
% Outputs:
%    Z - zonotope object
%
% Example: 
%    E = ellipsoid.generateRandom(0,2);
%    Z = insc_parallelotope(E);
%    plot(E);
%    hold on
%    plot(Z);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope

% Author:       Victor Gassmann, Matthias Althoff
% Written:      14-October-2019
% Last update:  27-January-2021 (MA, degenerate case implemented)
% Last revision:---

%------------- BEGIN CODE --------------
if ~E.isdegenerate
    T = inv(sqrtm(E.Q));
    n = length(E.Q);
    %transform ellipsoid into sphere -> square into sphere -> back transform
    Z = zonotope([E.q,inv(T)*1/sqrt(n)*eye(n)]);
else
    % compute eigenvalues to find axes of zero dimension
    [eigVec,eigVal] = eig(E.Q);
    
    % find non-zero eigenvalues
    ind = find(diag(eigVal)~=0);
    
    % obtain transformation matrix
    T = eigVec(:,ind);
    
    % compute enclosure for projected ellipsoid
    Zencl = insc_parallelotope(T'*E);
    
    % projection to original coordinates (transpose suffices due to orthogonal T)
    Z = T*Zencl;
end



%------------- END OF CODE --------------