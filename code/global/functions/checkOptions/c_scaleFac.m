function res = c_scaleFac(val,sys,options)
% c_scaleFac - costum validation function for options.factor
%
% Syntax:
%    res = c_scaleFac(val,sys,list)
%
% Inputs:
%    val - value for given param / option
%    sys - linearSys object
%    options - algorithm parameters
%
% Outputs:
%    res - logical whether validation was successful
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Mark Wetzlinger
% Written:      08-February-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% assume check ok
res = true;

if ischar(val) && ~strcmp(val,'auto')
    res = false; return
    
elseif isnumeric(val) && isscalar(val)
	if val <= 0 || val > 1
        res = false; return;
    end
    
else
    res = false; return;
    
end


end

%------------- END OF CODE --------------
