function res = c_pHA_finalLoc(val,sys,params)
% c_pHA_finalLoc - costum validation function for params.finalLoc
%
% Syntax:
%    res = c_pHA_finalLoc(val,sys,options)
%
% Inputs:
%    val - value for given param / option
%    sys - parallelHybridAutomaton object
%    params - model parameters
%
% Outputs:
%    res - logical whether validation was successful
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Mark Wetzlinger
% Written:      04-Februar-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% assume check ok
res = true;

numComp = length(sys.components);

if ~all(size(params.finalLoc) == [numComp,1])
    res = false; return;
else
    for c=1:numComp
        % loop through every component
        comp = sys.components{c};
        numLoc = length(comp.location);
        if params.finalLoc(c) > numLoc
            res = false; return;
        elseif params.finalLoc(c) < 0
            res = false; return;
        elseif mod(params.finalLoc(c),1.0) ~= 0
            res = false; return;
        end
    end
end


end

%------------- END OF CODE --------------


