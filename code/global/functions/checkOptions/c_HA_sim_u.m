function res = c_HA_sim_u(val,sys,params)
% c_HA_sim_u - costum validation function for params.U
%
% Syntax:
%    res = c_HA_sim_u(val,sys,options)
%
% Inputs:
%    val - value for given param / option
%    sys - hybridAutomaton object
%    params - model parameters
%
% Outputs:
%    res - logical whether validation was successful
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Mark Wetzlinger, Niklas Kochdumper
% Written:      04-February-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% assume check ok
res = true;

if ~iscell(params.u)
    
    % check if input has the correct format
    if size(params.u,2) ~= 1
        res = false;
    end
	
else
    % input is a cell
    
    locations = sys.location;
    numLoc = length(locations);
    % check if input matches the number of locations
    if all(size(params.u) ~= [numLoc,1]) && ...
       all(size(params.u) ~= [1,numLoc])
        res = false;
    end
    
    % check input for each location
    for i = 1:length(params.u)
        if size(params.u{i},2) ~= 1
            res = false;
        end
    end
    
end


end

%------------- END OF CODE --------------