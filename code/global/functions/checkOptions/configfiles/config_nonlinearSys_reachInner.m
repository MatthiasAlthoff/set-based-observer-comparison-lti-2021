function [paramsList,optionsList] = config_nonlinearSys_reachInner(sys,params,options)
% config_nonlinearSys_reachInner - configuration file for validation of
%    model parameters and algorithm parameters
%
% Syntax:
%    [paramsList,optionsList] = config_nonlinearSys_reachInner(sys,params,options)
%
% Inputs:
%    sys - nonlinearSys object
%    params - user-defined model parameters
%    options - user-defined algorithm parameters
%
% Outputs:
%    paramsList - list of model parameters
%    optionsList - list of algorithm parameters
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Mark Wetzlinger
% Written:      03-February-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% 1. init lists
initParamsOptionsLists();

% append entries to list of model parameters
add2params('R0','mandatory',{@(val)isa(val,'interval'),@(val)eq(dim(val),sys.dim)});
add2params('U','default',{@(val)any(ismember(getMembers('U'),class(val)))});
add2params('u','default',{@isnumeric});
add2params('tStart','default',{@isscalar,@(val)ge(val,0)});
add2params('tFinal','mandatory',{@isscalar,@(val)ge(val,params.tStart)});

% append entries to list of algorithm parameters
add2options('algInner','mandatory',{@ischar,@(val)any(ismember(getMembers('algInner'),val))});

add2options('timeStep','mandatory',{@isscalar,@(val)val>0});

% add2options('saveOrder','optional',{@isscalar,@(val)ge(val,1)});
add2options('verbose','default',{@isscalar,@islogical});
add2options('reductionTechnique','default',...
    {@ischar,@(val)any(ismember(getMembers('reductionTechnique'),val))});

% polyZono
add2options('polyZono.maxDepGenOrder','default',{@isscalar,@isnumeric,@(val)ge(val,1)});
add2options('polyZono.maxPolyZonoRatio','default',{@isscalar,@isnumeric,@(val)val>0});
add2options('polyZono.restructureTechnique','default',{@ischar,...
    @(val)any(ismember(getMembers('restructureTechnique'),val))});

% lagrangeRem
add2options('lagrangeRem.simplify','default',{@ischar,...
    @(val)any(ismember(getMembers('lagrangeRem.simplify'),val))});
add2options('lagrangeRem.method','default',{@ischar,...
    @(val)any(ismember(getMembers('lagrangeRem.method'),val))});
add2options('lagrangeRem.tensorParallel','default',{@isscalar,@islogical});
add2options('lagrangeRem.replacements','optional',{@(val)isa(val,'function_handle')});
add2options('lagrangeRem.zooMethods','mandatory',...
    {@ischar,@(val)any(ismember(getMembers('lagrangeRem.zooMethods'),val))},...
    {@()strcmp(options.lagrangeRem.method,'zoo')});
add2options('lagrangeRem.optMethod','default',...
    {@ischar,@(val)any(ismember(getMembers('lagrangeRem.optMethod'),val))},...
    {@()strcmp(options.lagrangeRem.method,'taylorModel')});
add2options('lagrangeRem.maxOrder','optional',...
    {@isscalar,@isnumeric,@(val)mod(val,1)==0,@(val)ge(val,1)},...
    {@()any(strcmp(options.lagrangeRem.method,{'taylorModel','zoo'}))});
add2options('lagrangeRem.tolerance','optional',{@isscalar,@isnumeric,@(val)ge(val,0)},...
    {@()any(strcmp(options.lagrangeRem.method,{'taylorModel','zoo'}))});
add2options('lagrangeRem.eps','optional',{@isscalar,@isnumeric,@(val)ge(val,0)},...
    {@()any(strcmp(options.lagrangeRem.method,{'taylorModel','zoo'}))});

% only algInner = 'parellelo' or 'scale'
add2options('taylorTerms','mandatory',{@isscalar,@(val)mod(val,1)==0,@(val)ge(val,1)},...
    {@()any(strcmp(options.algInner,{'parallelo','scale'}))});
add2options('zonotopeOrder','mandatory',{@isscalar,@(val)ge(val,1)},...
    {@()any(strcmp(options.algInner,{'parallelo','scale'}))});

% only algInner = 'parallelo'
add2options('reductionInterval','default',{@isscalar,@(val)ge(val,1),@(val)isinf(val)||mod(val,1)==0},...
    {@()strcmp(options.algInner,'parallelo')});
add2options('maxError','default',{@isvector,@(val)all(ge(val,0)),@(val)length(val)==sys.dim},...
    {@()strcmp(options.algInner,'parallelo')});
add2options('alg','mandatory',{@ischar,@(val)any(ismember(getMembers('alg'),val))},...
    {@()strcmp(options.algInner,'parallelo')});
add2options('tensorOrder','mandatory',...
    {@isscalar,@(val)mod(val,1)==0,@(val)c_tensorOrder(val,sys,options)},...
    {@()strcmp(options.algInner,'parallelo')});
add2options('errorOrder','mandatory',{@isscalar,@(val)ge(val,1)},...
    {@()(strcmp(options.algInner,'parallelo') && options.tensorOrder>2) || ...
    strcmp(options.algInner,'scale')});
add2options('errorOrder3','mandatory',{@isscalar,@(val)ge(val,1)},...
    {@()strcmp(options.algInner,'parallelo') && options.tensorOrder>3});
add2options('intermediateOrder','mandatory',{@isscalar,@(val)ge(val,1)},...
    {@()strcmp(options.algInner,'parallelo') && options.tensorOrder>2 || ...
    strcmp(options.algInner,'scale')});

% only algInner = 'proj'
add2options('taylorOrder','mandatory',{@isscalar,@isnumeric,@(val)mod(val,1)==0,@(val)ge(val,1)},...
    {@()strcmp(options.algInner,'proj')});
add2options('taylmOrder','mandatory',{@isscalar,@isnumeric,@(val)mod(val,1)==0,@(val)ge(val,1)},...
    {@()strcmp(options.algInner,'proj')});

% only algInner = 'scale'
add2options('timeStepInner','optional',{@isscalar,@isnumeric,@(val)abs(rem(params.tFinal,val))<eps},....
    {@()strcmp(options.algInner,'scale')});
add2options('contractor','default',{@ischar,@(val)any(ismember(getMembers('contractor'),val))},...
    {@()strcmp(options.algInner,'scale')});
add2options('iter','default',{@isscalar,@isnumeric,@(val)mod(val,1)==0,@(val)ge(val,1)},...
    {@()strcmp(options.algInner,'scale')});
add2options('splits','default',{@isscalar,@isnumeric,@(val)mod(val,1)==0,@(val)ge(val,1)},...
    {@()strcmp(options.algInner,'scale')});
add2options('scaleFac','default',{@(val)c_scaleFac(val,sys,options)},{@()strcmp(options.algInner,'scale')});
add2options('orderInner','default',{@isscalar,@isnumeric,@(val)ge(val,1)},...
    {@()strcmp(options.algInner,'scale')});

% 3. prepare lists for output args
[paramsList,optionsList] = outputParamsOptionsLists();

end

%------------- END OF CODE --------------

