function [paramsList,optionsList] = config_linearSysDT_observe(sys,params,options)
% config_linearSysDT_observe - configuration file for validation of
%    model parameters and algorithm parameters
%
% Syntax:
%    [paramsList,optionsList] = config_linearSysDT_observe(sys,params,options)
%
% Inputs:
%    sys - hybridSystem or contDynamics object
%    params - user-defined model parameters
%    options - user-defined algorithm parameters
%
% Outputs:
%    paramsList - list of model parameters
%    optionsList - list of algorithm parameters
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      ---
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% 1. init lists
initParamsOptionsLists();

% append entries to list of model parameters
add2params('tStart','default',{@isscalar,@(val)ge(val,0)});
add2params('tFinal','mandatory',{@isscalar,@(val)ge(val,params.tStart)});

% append entries to list of algorithm parameters
add2options('verbose','default',{@isscalar,@islogical});


% 3. prepare lists for output args
[paramsList,optionsList] = outputParamsOptionsLists();

end

%------------- END OF CODE --------------

