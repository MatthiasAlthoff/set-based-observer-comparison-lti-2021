function Rint = observe_intersectionMethod_II(obj,R,y,options)
% observe_intersectionMethod_II - intersects the reachable set with
% measurement strips according to intersection method II in [1]. 
%
%
% Syntax:  
%    Rint = observe_intersectionMethod_II(obj,R,y,options)
%
% Inputs:
%    obj - discrete-time linear system object
%    R - reachable set
%    y - current measurement
%    options - options for the computation
%
% Outputs:
%    Rint - resulting zonotope after intersections with strips
%
% Reference:
%    [1] M. Althoff and J. J. Rath. Comparison of Set-Based Techniques 
%        for Guaranteed State Estimation of Linear Disturbed Systems, 
%        in preparation.
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Matthias Althoff
% Written:       18-Sep-2020
% Last update:   02-Mar-2021
% Last revision: ---

%------------- BEGIN CODE --------------

% intersection of zonotope with strips
Rint = intersectStrip(R,obj.C,options.sigma,y,options.intersectionTechnique);


%------------- END OF CODE --------------